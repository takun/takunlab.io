---
title: "WebAssembly"
date: 2021-10-29
tags: ["WebAssembly", "Rust"]
---

# WebAssembly

## wasm-bindgen

### wasm-pack をインストール

```
cargo install wasm-pack
```

### wasm-pack でビルド

```
wasm-pack build --target web
```
