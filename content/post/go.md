---
title: "Go"
date: 2021-01-21
tags: ["Go"]
---

# Go

## go mod

[Using Go Modules](https://blog.golang.org/using-go-modules)
[go mod init <ここには何を書く？>](https://teratail.com/questions/217859)

```
$ # go mod init 公開しなければこちらはなんでもいいらしい
$ # 例
$ go mod init hello
```

## go test
